#pragma once

class Calc
{
public:
	explicit Calc(int multiplier);

	int add(int a, int b);
	
	double add(double a, double b);
	
	int multiply(int a);

private:
	int multiplier;
};