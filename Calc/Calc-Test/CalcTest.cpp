#include <gtest/gtest.h>
#include "../Calc/Calc.h"

class CalcTest: public testing::Test
{
protected:
	void SetUp() override
	{
		calc = new Calc(42);
	}
	
	void TearDown() override
	{
		delete calc;
	}

public:
	Calc* calc;
};

TEST_F(CalcTest, test_positive_add)
{
	ASSERT_EQ(42, calc->add(40, 2));
	ASSERT_EQ(-38, calc->add(-40, 2));
	ASSERT_DOUBLE_EQ(12.345, calc->add(12.0, 0.345));
}